package jtm.activity03;

public interface ReadOnlyPerson {

	String getName();

	int getAge();

	float getWeight();

	boolean isMale();

	char getSmile();

}