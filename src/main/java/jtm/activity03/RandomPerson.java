package jtm.activity03;

public class RandomPerson implements ReadOnlyPerson {

	// TODO Create _private_ structure of random person to store values:
	
	private String name;
	private int age;
	private float weight;
	private boolean isMale;
	private char smile;
	
	@Override
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public int getAge() {
		return age;
	}
	
	public void setAge(int age) {
		this.age = age;
	}
	
	@Override
	public float getWeight() {
		return weight;
	}
	
	public void setWeight(float weight) {
		this.weight = weight;
	}
	
	@Override
	public boolean isMale() {
		return isMale;
	}
	
	public void setMale(boolean isMale) {
		this.isMale = isMale;
	}
	
	@Override
	public char getSmile() {
		return smile;
	}
	
	public void setSmile(char smile) {
		this.smile = smile;
	}

	@Override
	public String toString() {
		return "RandomPerson [name=" + name + ", age=" + age + ", weight=" + weight + ", isMale=" + isMale + ", smile="
				+ smile + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + age;
		result = prime * result + (isMale ? 1231 : 1237);
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + smile;
		result = prime * result + Float.floatToIntBits(weight);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RandomPerson other = (RandomPerson) obj;
		if (age != other.age)
			return false;
		if (isMale != other.isMale)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (smile != other.smile)
			return false;
		if (Float.floatToIntBits(weight) != Float.floatToIntBits(other.weight))
			return false;
		return true;
	}
	
	
	// name as String,
	// age as int,
	// weight as float,
	// isFemale as boolean;
	// smile as char
	// HINT: use Alt+Shift+A to switch to Block selection (multi-line cursor)
	// to edit list of all properties at once

	// TODO Select menu "Source — Generate Getters and Setters..." then select
	// all properties and generate _public_ getters and setters for all of them
}
