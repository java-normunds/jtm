package jtm.activity03;

import java.util.Arrays;

public class Array {
	static int[] array;

	public static void main(String[] args) {
		int[] arrayOfInt = new int[args.length];
		for( int n=0 ; n<args.length ; n++ ) {
			arrayOfInt[n] = Integer.parseInt(args[n]); 
		}
		Arrays.sort(arrayOfInt);
		printSortedArray(arrayOfInt);
		// TODO Use passed parameters for main method to initialize array
		// Hint: use Run— Run configurations... Arguments to pass parameters to
		// main method when calling from Eclipse
		// Sort elements in this array in ascending order
		// Hint: use cto convert passed
		// parameters from String to int
		//) Hint: use Arrays.sort(...) from
		// https://docs.oracle.com/javase/7/docs/api/java/util/Arrays.html
	}

	public static void printSortedArray(int[] arrayOfInt) {
		System.out.println(Arrays.toString(arrayOfInt));
		// TODO print content of array on standard output
		// Hint: use Arrays.toString(array) method for this
	}

	public static int[] returnSortedArray(int[] arrayOfInt) {
		// TODO return reference to this array
		return null;
	}

}
