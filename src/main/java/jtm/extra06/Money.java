package jtm.extra06;

public class Money {

	private final Currency currency;

	private int amount = 0;

	public Money(Currency currency) {
		this.currency = currency;
	}

	public Currency getCurrency() {
		return currency;
	}

	public int getAmount() {
		return amount;
	}

	public void addAmount(int add) {
		if (add <= 0) {
			throw new IllegalArgumentException("You can add only positive amount");
		}
		amount += add;
	}

	public int tryToRemoveAmount(int remove) {
		if (remove <= 0) {
			throw new IllegalArgumentException("You can remove only positive amount");
		}
		int actualRemove = Math.min(amount, remove);
		amount -= actualRemove;
		return actualRemove;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((currency == null) ? 0 : currency.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Money other = (Money) obj;
		if (currency != other.currency)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Money [currency=" + currency + ", amount=" + amount + "]";
	}

}
