package jtm.extra06;

public class WalletUsage {

	public static void main(String[] args) {
		Wallet redWallet = new Wallet();
		Wallet greenWallet = new Wallet();
		
		System.out.println(redWallet);
		
		redWallet.addAmount(Currency.RUB, 100);
		redWallet.addAmount(Currency.EUR, 200);
		
		System.out.println(redWallet);
		stealSomeMoney(redWallet);
		System.out.println(redWallet);
		
		System.out.println(greenWallet);
		greenWallet.addAmount(Currency.USD, 2);
		stealSomeMoney(greenWallet);
		System.out.println(greenWallet);


	}
	
	static void stealSomeMoney(Wallet myWallet) {
		int stolenUSD = myWallet.tryToRemoveAmount(Currency.USD, 1);
		System.out.println("stolenUSD: " + stolenUSD);
	}

}
