package jtm.extra06;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;

public class LetsPrint {

	public static void main(String[] args) {
		List<Integer> listOfIntegers = new ArrayList<>();
		listOfIntegers.add(1);
		listOfIntegers.add(2);
		listOfIntegers.add(3);
		listOfIntegers.forEach(conditionalPrinter(3));
		
		Optional<Integer> opt = Optional.empty();
		
		AtomicInteger outside =new AtomicInteger(1);
		Generics<Number> run = new Generics<Number>() {
			
			private int f = outside.get();

			@Override
			public Number pop() {
				outside.set(2);
				
				return super.pop();
			}
			
			
			
		};
		
		outside.get();
		
	}
	
	static Consumer<Integer> conditionalPrinter(int atLeast) {
		return t-> {
			if( t >= atLeast) 
				System.out.println(t);	
		};
	}

}
