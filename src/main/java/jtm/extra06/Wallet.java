package jtm.extra06;

import java.util.HashSet;
import java.util.Set;

public class Wallet {
	
	// TODO name field

	private final Set<Money> allMoney = new HashSet<>();

	public int getAmount(Currency currency) {
		return findOrCreateMoney(currency).getAmount();
	}

	public void addAmount(Currency currency, int amount) {
		findOrCreateMoney(currency).addAmount(amount);
	}

	public int tryToRemoveAmount(Currency currency, int amount) {
		return findOrCreateMoney(currency).tryToRemoveAmount(amount);
	}

	private Money findOrCreateMoney(Currency currency) {
		for (Money money : allMoney) {
			if (money.getCurrency() == currency) {
				return money;
			}
		}
		Money money = new Money(currency);
		allMoney.add(money);
		return money;
	}

	@Override
	public String toString() {
		return "Wallet [allMoney=" + allMoney + "]";
	}

}
