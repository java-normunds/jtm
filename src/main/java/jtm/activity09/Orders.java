package jtm.activity09;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import jtm.extra11.UsesObjects;
import org.apache.commons.lang3.tuple.Pair;

/*- TODO #2
 * Implement Iterator interface with Orders class
 * Hint! Use generic type argument of iterateable items in form: Iterator<Order>
 * 
 * TODO #3 Override/implement public methods for Orders class:
 * - Orders()                — create new empty Orders
 * - add(Order item)            — add passed order to the Orders
 * - List<Order> getItemsList() — List of all customer orders
 * - Set<Order> getItemsSet()   — calculated Set of Orders from list (look at description below)
 * - sort()                     — sort list of orders according to the sorting rules
 * - boolean hasNext()          — check is there next Order in Orders
 * - Order next()               — get next Order from Orders, throw NoSuchElementException if can't
 * - remove()                   — remove current Order (order got by previous next()) from list, throw IllegalStateException if can't
 * - String toString()          — show list of Orders as a String
 * 
 * Hints:
 * 1. To convert Orders to String, reuse .toString() method of List.toString()
 * 2. Use built in List.sort() method to sort list of orders
 * 
 * TODO #4
 * When implementing getItemsSet() method, join all requests for the same item from different customers
 * in following way: if there are two requests:
 *  - ItemN: Customer1: 3
 *  - ItemN: Customer2: 1
 *  Set of orders should be:
 *  - ItemN: Customer1,Customer2: 4
 */

public class Orders implements Iterable<Order> {
	/*-
	 * TODO #1
	 * Create data structure to hold:
	 *   1. some kind of collection of Orders (e.g. some List)
	 *   2. index to the current order for iterations through the Orders in Orders
	 *   Hints:
	 *   1. you can use your own implementation or rely on .iterator() of the List
	 *   2. when constructing list of orders, set number of current order to -1
	 *      (which is usual approach when working with iterateable collections).
	 */
	
	private List<Order> orderList = new ArrayList<>();
	
	@Override
	public Iterator<Order> iterator() {
		return new Iterator<Order>() {
			
			private int index = -1;

			@Override
			public boolean hasNext() {
				return index+1 < orderList.size();
			}

			@Override
			public Order next() {
				if( !hasNext() ) {
					throw new NoSuchElementException();
				}
				index++;
				return orderList.get(index);
			}
			
			@Override
			public void remove() {
				if( index<0 || index>=orderList.size() ) {
					throw new IllegalStateException();
				}
				orderList.remove(index);
			}
			
		};
	}

	public void add(Order item) {
		orderList.add(item);
	}
	
	public List<Order> getItemsList() {
		return new ArrayList<>(orderList);
	}
		
	public Set<Order> getItemsSet() {
		Map<Pair<String,String>, Order> map = new HashMap<>();
		for( final Order order : orderList ) {
			Order valueOrder = map.computeIfAbsent(Pair.of(order.getCustomer(), order.getName()), k->order);
		}
		
		return new HashSet<>(orderList);
	}

	public void sort() {
		Collections.sort(orderList);
	}
	
	@Override
	public String toString() {
		return ">>>" + orderList.toString();
	}
	
	public static void main(String[] args) throws ParseException {
		SimpleDateFormat simple = new SimpleDateFormat("yyyy-MM-dd");
		Date date = simple.parse("2019-11-01");

		System.out.println(date);

		UsesObjects u = new UsesObjects(null, null);

		Orders pasūtījumi = new Orders();
		pasūtījumi.add(new Order("Accenture", "Java Bootcamp", 3));
		pasūtījumi.add(new Order("Accenture", ".NET Bootcamp", 2));
		pasūtījumi.add(new Order("Accenture", "DEVOPS", 5));
		pasūtījumi.add(new Order("Accenture", "Java Bootcamp", 13));
		pasūtījumi.add(new Order("Accenture", "DEVOPS", 1));


		System.out.println("Visi pasūtījumi: " + pasūtījumi);
		
		int kopējaisPasūtījumuSkaits = 0;
		Iterator<Order> iterator = pasūtījumi.iterator();
		while(iterator.hasNext()) {
			Order pasūtījums = iterator.next();
			kopējaisPasūtījumuSkaits += pasūtījums.getCount();
		}
		System.out.println("Pasūtījumu kopējais skaits: " + kopējaisPasūtījumuSkaits);

		kopējaisPasūtījumuSkaits = 0;
		for( Order pasūtījums:  pasūtījumi ) {
			kopējaisPasūtījumuSkaits += pasūtījums.getCount();
		}
		System.out.println("Pasūtījumu kopējais skaits: " + kopējaisPasūtījumuSkaits);
		System.out.println("Katrs unikālais elements: " + pasūtījumi.getItemsSet());
		
		kopējaisPasūtījumuSkaits = 0;
		for( Order pasūtījums:  pasūtījumi.getItemsSet() ) {
			kopējaisPasūtījumuSkaits += pasūtījums.getCount();
		}
		System.out.println("Unikālais Pasūtījumu kopējais skaits: " + kopējaisPasūtījumuSkaits);
		
		List a = new ArrayList();
		a.add(a);
		System.out.println(a);
		
		
	}
	
}
