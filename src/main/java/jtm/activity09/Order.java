package jtm.activity09;

/*- TODO #1
 * Implement Comparable interface with Order class
 * Hint! Use generic type of comparable items in form: Comparable<Order>
 * 
 * TODO #2 Override/implement necessary methods for Order class:
 * - public Order(String orderer, String itemName, Integer count) — constructor of the Order
 * - public int compareTo(Order order) — comparison implementation according to logic described below
 * - public boolean equals(Object object) — check equality of orders
 * - public int hashCode() — to be able to handle it in some hash... collection 
 * - public String toString() — string in following form: "ItemName: OrdererName: Count"
 * 
 * Hints:
 * 1. When comparing orders, compare their values in following order:
 *    - Item name
 *    - Customer name
 *    - Count of items
 * If item or customer is closer to start of alphabet, it is considered "smaller"
 * 
 * 2. When implementing .equals() method, rely on compareTo() method, as for sane design
 * .equals() == true, if compareTo() == 0 (and vice versa).
 * 
 * 3. Also Ensure that .hashCode() is the same, if .equals() == true for two orders.
 * 
 */

public final class Order implements Comparable<Order> {
	
	private final String customer; // Name of the customer
	private final String name; // Name of the requested item
	private final int count; // Count of the requested items
	
	public Order(String customer, String name, int count) {
		if( customer==null || customer.isEmpty() ) {
			throw new IllegalArgumentException("customer is empty");
		}
		if( name==null || name.isEmpty() ) {
			throw new IllegalArgumentException("name is empty");
		}
		if( count<1 ) {
			throw new IllegalArgumentException("count is less than 1");
		}
		this.customer = customer;
		this.name = name;
		this.count = count;
	}

	public String getCustomer() {
		return customer;
	}

	public String getName() {
		return name;
	}

	public int getCount() {
		return count;
	}

	@Override
	public int compareTo(Order o) {
		int r;
		r = name.compareTo(o.name);
		if( r==0 ) {
			r = customer.compareTo(o.customer);
		}
		if( r==0 ) {
			r = count<o.count ? -1 : count>o.count ? +1 : 0;
			// if(count<o.count) -1 else if(count>o.count) +1 else 0
		}		
		return r;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + count;
		result = prime * result + ((customer == null) ? 0 : customer.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Order other = (Order) obj;
		return compareTo(other) == 0;
	}

	@Override
	public String toString() {
		return "Order [customer=" + customer + ", name=" + name + ", count=" + count + "]";
	}
	
	public static void main(String[] args) {
		System.out.println( new Order("Edgars", "Normunds", 1) );
	}
	
}
