package jtm.activity08;

public class IllegalAgeException extends Exception {
	
	int age;

	public IllegalAgeException(int age) {
		this.age = age;
	}

	public int getAge() {
		return age;
	}

	@Override
	public String toString() {
		return "IllegalAgeException [age=" + age + "]";
	}
	
	

}
