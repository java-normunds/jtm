package jtm.activity08;

import java.util.Scanner;

public class AgeCheck {

	public static void main(String[] args) {
		int age;
		try (Scanner scanner = new Scanner(System.in)) {
			age = ageInput(scanner);
		}
		System.out.println("User have entered age: " + age);
	}

	private static int ageInput(Scanner scanner) {
		while (true) {	
			try {
				System.out.println("Please enter age: ");
				String input = scanner.next();
				int age = Integer.parseInt(input);
				if (age < 18) {
					throw new IllegalAgeException(age);
				}
				return age;
			} catch (IllegalAgeException e) {
				int age = e.getAge();
				if( age < 6 ) {
					throw new IllegalArgumentException("You are underage!", e);
				}
				System.out.println(
						String.format("You have entered wrong age %d. Please enter legal age instead", age));
			} catch (NumberFormatException e) {
				System.out.println("Please enter number");
			}
		}
	}

}
