package jtm.extra11;

public class NameAndAge {
	
	private final String name;
	private final int age;
	
	public NameAndAge(String name, int age) {
		this.name = name;
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public int getAge() {
		return age;
	}

	@Override
	public String toString() {
		return "NameAndAge [name=" + name + ", age=" + age + "]";
	}
	

}
