package jtm.extra11;

import java.util.*;
import java.util.stream.*;

import jtm.activity03.RandomPerson;
import jtm.activity03.ReadOnlyPerson;

public class PersonMain {

	public static void main(String[] args) {
		Collection<ReadOnlyPerson> collection = createCollection();
		
//		Set<ReadOnlyPerson> outcome =(Set<ReadOnlyPerson>) produceOutcome(collection, Collectors.toSet());
//		ReadOnlyPerson person = outcome.iterator().next();
//		person.getAge();
//		((RandomPerson)person).setAge(12);
//		
//		System.out.println(outcome);
		
		List<NameAndAge> nameAndAgeCollection = collection.stream()
				.map( p-> new NameAndAge(p.getName(), p.getAge()) ).collect(Collectors.toList());
		
		System.out.println(nameAndAgeCollection);

	}

	private static Collection<ReadOnlyPerson> createCollection() {
		Collection<ReadOnlyPerson> collection = new ArrayList<>();
		RandomPerson person1 = new RandomPerson();
		person1.setAge(17);
		person1.setMale(false);
		person1.setSmile(')');
		person1.setWeight(50f);
		person1.setName("Zane");
		RandomPerson person2 = new RandomPerson();
		person2.setAge(18);
		person2.setMale(true);
		person2.setSmile(')');
		person2.setWeight(70f);
		person2.setName("Jānis");
		RandomPerson person3 = new RandomPerson();
		person3.setAge(18);
		person3.setMale(true);
		person3.setSmile(')');
		person3.setWeight(70f);
		person3.setName("Jānis");
		
		collection.add(person1);
		collection.add(person2);
		collection.add(person2);
		collection.add(person3);
		return collection;
	}

	private static <C extends Collection<ReadOnlyPerson>> C produceOutcome(Collection<ReadOnlyPerson> collection,
			Collector<ReadOnlyPerson, ?, C> collector) {
		return (C)collection.stream().filter(p->p.isMale()).collect(collector);
	}

}
