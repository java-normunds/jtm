package jtm.extra11;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import jtm.activity03.RandomPerson;
import jtm.activity03.ReadOnlyPerson;

public interface PersonMatcher {
	
	

	void addPerson(ReadOnlyPerson person);

	List<RandomPerson> getPersonList();
	
	Stream<RandomPerson> getPersonStream();
	
	static Stream<RandomPerson> getMatchedPersonStream(Stream<RandomPerson> persons, 
			boolean isFemale, int ageFrom, int ageTo,float weightFrom,float weightTo) {
		
		Predicate<RandomPerson> predicate = new Predicate<RandomPerson>() {

			@Override
			public boolean test(RandomPerson p) {
				return  p.isMale()!=isFemale;
			}
		
		};
		
		
		return persons.filter(predicate)
				.filter(p->p.getAge()>=ageFrom).filter(p->p.getAge()<=ageTo)
				.filter(p->p.getWeight()>=weightFrom).filter(p->p.getWeight()<=weightTo);
	}


}
