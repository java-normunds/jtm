package jtm.extra11;

import jtm.activity09.Order;

import java.util.Optional;

public class UsesObjects {

    private final Optional<Order> order1;
    private final Optional<Order> order2;
    private int total;

    public UsesObjects(Order order1, Order order2) {
        this.order1 = Optional.ofNullable(order1);
        this.order2 = Optional.ofNullable(order2);
        calculate();
    }

    public void calculate() {
        if( order2==null ) {
            throw new IllegalStateException("order1 is null");
        }
        int fromOrder1 = order1.map(Order::getCount).orElse(0);
        int fromOrder2 = order2.map(Order::getCount).orElse(0);
        total = fromOrder1 + fromOrder2;
    }

    public int getTotal() {
        return total;
    }

}
