package jtm.activity04;

import jtm.activity05.Ship;
import jtm.activity05.WaterRoad;
import jtm.activity06.Alien;
import jtm.activity06.Humanoid;

public class Amphibia implements Moveable, Alien {
	
	private Ship ship;
	
	private Vehicle vehicle;

	public Amphibia(String id, byte sails, float consumption, int tankSize) {
		this.ship = new Ship(id, sails);
		this.vehicle = new Vehicle(id, consumption, tankSize);
	}
	
	@Override
	public String move(Road road) {
		if( road instanceof WaterRoad) {
			return ship.move(road);
		} else {
			return vehicle.move(road);
		}
	}

	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return "Amphibia";
	}

	@Override
	public int getWeight() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void setWeight(int weight) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void eatHuman(Humanoid humanoid) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int getLegCount() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Object getBackpack() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setBackpack(Object item) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String isAlive() {
		// TODO Auto-generated method stub
		return null;
	}

}
