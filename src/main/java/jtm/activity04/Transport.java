package jtm.activity04;

import java.util.Locale;

import jtm.activity05.WaterRoad;

public class Transport implements Moveable {
	// Do not change access modifiers to encapsulate internal properties!
	private String id; // Transport registration number
	private float consumption; // fuel consumption in liters per 100km
	private int tankSize; // tank size in liters
	private float fuelInTank; // fuel in tank

	public Transport(String id, float consumption) {
		this.id = id;
		this.consumption = consumption;

	}

	public Transport(String id, float consumption, int tankSize) {
		this.id = id;
		this.consumption = consumption;
		this.tankSize = tankSize;
		this.fuelInTank = tankSize;
	}

	public static void main(String[] args) {
		Moveable transport = createSomethingMoveable();
		System.out.println(transport.getType());
		Road road = new WaterRoad();
		
		System.out.println("transport before move: " + transport);
		String moveResult = transport.move(road);
		System.out.println("move: " + moveResult);
		System.out.println("transport after move: " + transport);
	}

	private static Moveable createSomethingMoveable() {
//		return new Vehicle("BMW", 12.0f, 1000);
		return new Amphibia("Amph", (byte)2, 1, 10000);
	}

	/*- TODO #1
	 * Select menu Source — Generate Constructor using Fields...
	 * and create constructor which sets id, consumption, tankSize
	 * values of the newly created object
	 * And make fuel tank full.
	 */

	/*- TODO #2
	 * Select menu: Source — Generate getters and Setters...
	 * and generate public getters for consumption, tankSize, id, and
	 * fuelInTank fields
	 */

	/*- TODO #3
	 * Select menu: Source — Generate toString()...
	 * and implement this method, that t returns String in form:
	 * "Id:ID cons:0.0l/100km, tank:00l, fuel:00.00l"
	 * where ID and numbers are actual numbers of the transport
	 * HINT: use String.format(Locale.US, "%.2f", float) to limit shown numbers
	 * to 2 decimal for fractions, and dot as a decimal delimiter.
	 */

	// Return transport id and type as string e.g. "AAA Transport"
	// HINT: use this.getClass().getSimpleName(); to get type of transport
	public final String getType() {
		// TODO return required value
		return String.format("%s %s", id, this.getClass().getSimpleName());
	}

	@Override
	public String toString() {
		return String.format("Id:%s cons:%.2fl/100km, tank:%dl, fuel:%.2fl", id, consumption, tankSize, fuelInTank);
	}

	// HINT: use getType() to describe transport and road.toString() to describe
	// road
	// HINT: String.format(Locale.US, "%.2f", float) to format float number with
	// fixed mask
	@Override
	public String move(Road road) {
		// TODO If transport has enough fuel, decrease actual amount of fuel by
		// necessary amount and return String in form:
		// "AAA Type is moving on From–To, 180km"
		// TODO If there is no enough fuel in tank, return string in form:
		// "Cannot move on From–To, 180km. Necessary
		// fuel:0.00l, fuel in tank:0.00l"
		int distance = road.getDistance();
		float totalDistanceCanMove = fuelInTank * consumption / 100f;
		final String message;
		float fuelNeedToMove = distance  / 100.0f * consumption;
		if (totalDistanceCanMove >= fuelNeedToMove) {
			fuelInTank -= fuelNeedToMove;
			message = String.format("%s is moving on From–To, %dkm", getType(), distance);
		} else {
			message = String.format(Locale.US, "Cannot move on From–To, %dkm. Necessary fuel:%.2fl, fuel in tank:%.2fl",
					distance, fuelNeedToMove, fuelInTank);
		}
		return message;
	}

}
