package jtm.activity04;

import java.util.Scanner;

public class Road {
	private String from; // Start point
	private String to; // End point
	private int distance; // distance in km
	
	
	
	public Road(String no, String to, int distance) {
		super();
		this.from = no;
		this.to = to;
		this.distance = distance;
	}



	public Road(String from) {
		super();
		this.from = from;
		to = null;
		distance = 10000;
	}


	public Road() {
		this(null, null, 0);
	}



	@Override
	public String toString() {
		return String.format("%s — %s, %dkm", from, to, distance);
	}
	
	public static void main(String args[]) {
		Road road = new Road("Riga");
		System.out.println(road.toString());
		Transport vehicle = new Vehicle("bmw",2f);
		vehicle.move(road);
	}



	public String getFrom() {
		return from;
	}



	public void setFrom(String from) {
		this.from = from;
	}



	public String getTo() {
		return to;
	}



	public void setTo(String to) {
		this.to = to;
	}



	public int getDistance() {
		return distance;
	}



	public void setDistance(int distance) {
		this.distance = distance;
	}

	
	
	/*- TODO #1
	 * Select menu Source — Generate Constructor using Fields...
	 * and create constructor which sets from, to and distance
	 * values of the newly created object
	 */

	/*- TODO #2
	 * Create constructor without parameters, which sets empty
	 * values or 0 to all object properties
	 */


	/*- TODO #3
	 * Select menu: Source — Generate getters and Setters...
	 * and generate public getters/setters for distance, from and to
	 * fields
	 */


	/*- TODO #4
	 * Select menu: Source — Generate toString()...
	 * and implement this method, that it returns String in form:
	 * "From — To, 00km",
	 * where "From" is actual from point, "To" — actual to point and
	 * 00 is actual length of the road
	 * Note that — is not dash ("minus key" in jargon), but m-dash!
	 * See more at: https://en.wikipedia.org/wiki/Dash
	 */


}
