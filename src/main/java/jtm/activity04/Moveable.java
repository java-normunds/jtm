package jtm.activity04;

public interface Moveable {

	String move(Road road);
	
	String getType() ;

}