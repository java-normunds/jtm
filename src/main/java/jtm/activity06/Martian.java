package jtm.activity06;

public class Martian implements Humanoid, Alien, Cloneable {
	
	private static final String I_AM_IMMORTAL = "I AM IMMORTAL!";
	
	private Object backpack = null;
	private int weight ;
	
	public Martian(int weight) {
		this.weight = weight;
	}

	@Override
	public Martian clone() {
		return clone(this);
	}
	
	private Martian clone(Object current) {
		final Object newBackpack;
		if(backpack!=null) {
			if( backpack instanceof Martian ) {
				newBackpack = ((Martian)backpack).clone();
			} else if( backpack instanceof String ) {
				newBackpack = backpack;
			} else {
				newBackpack = null;
			}
		} else {
			newBackpack = null;
		}
	    // TODO implement cloning of current object
	    // and its backpack
		Martian newMartian = new Martian(weight);
		newMartian.setBackpack(newBackpack);
		return newMartian;
	}
	
	@Override
	public void eatHuman(Humanoid humanoid) {
		if(!humanoid.isAlive().equals(I_AM_IMMORTAL)) {
			int humanoidWeight = humanoid.getWeight();
			this.weight = this.weight + humanoidWeight;
			humanoid.killHimself();
		}
	}

	@Override
	public int getLegCount() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void setBackpack(Object item) {
		backpack = item;
	}

	@Override
	public int getWeight() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void setWeight(int weight) {
		// TODO Auto-generated method stub

	}

	@Override
	public String killHimself() {
		// TODO Auto-generated method stub
		return I_AM_IMMORTAL;
	}

	@Override
	public int getArmCount() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Object getBackpack() {
		return clone(this);
	}

	@Override
	public void setBackpack(String item) {
		backpack = item;
	}

	@Override
	public String isAlive() {
		return I_AM_IMMORTAL;
	}
	
	@Override
	public String toString() {
		return "Martian";
	}

}
