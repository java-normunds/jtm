package jtm.activity06;

public class TestaKaķis implements Galvena.Kakis {

	private String kaķaVārds;

	public TestaKaķis(String padotaisKaķaVārds) {
		if (padotaisKaķaVārds == null || "".equals(padotaisKaķaVārds)) {
			System.out.println("Vai vai - kaķīša vārds nevar būt tukšs vai null - nabaga kaķītis");
		}
		kaķaVārds = kaķaVārds.toLowerCase();
		kaķaVārds = padotaisKaķaVārds;
	}

	@Override
	public int kajas() {
		return 4;
	}

	@Override
	public String vards() {
		return kaķaVārds;
	}

	@Override
	public String toString() {
		return "TestaKaķis [kaķaVārds=" + kaķaVārds + "]";
	}

	public static void main(String[] args) {
		Galvena.Kakis labais_kaķis = new TestaKaķis("labs");
		System.out.println(labais_kaķis);
		Galvena.Kakis sliktais_kaķis = new TestaKaķis(null);
		System.out.println( String.valueOf(sliktais_kaķis) );
		
	}

}